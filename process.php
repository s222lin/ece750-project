<?php
session_start();

// Check if the reset button is clicked
if (isset($_POST["reset"])) {
    // Unset all session variables
    session_unset();

    // Destroy the session
    session_destroy();

    // Redirect to the index.html page
    header("Location: index.html");
    exit;
}

// Initialize the array to store user information and request counts
if (!isset($_SESSION['userData'])) {
    $_SESSION['userData'] = array_fill(0, 50, ['username' => '', 'requestCount' => 0]);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    // Read the file content into a string
    $accessContent = file_get_contents("block.log");

    // Explode the string into an array using newline as the delimiter
    $accessContentArray = explode("\n", $accessContent);

    // Boolean variable to indicate if a match is found
    $matchFound = false;
    $username = $_POST["username"];

    // Loop through each element in the array
    foreach ($accessContentArray as $element) {
        
        // Compare the string variable with the current array element
        if ($username === substr_replace($element ,"", -1)) {
            // Match found, set the boolean variable to true and break out of the loop
            $matchFound = true;
            break;
        }
    }

    if ($matchFound) {
        echo "<h2>Blocked, $username!</h2>";
    }
    else {
        
        $timestamp = time();
        $historyHandle = fopen("history.log", 'a');
        fwrite($historyHandle, "$username,$timestamp\n");
        fclose($historyHandle);
        // Simulate a 200ms delay using usleep
        usleep(200000);
        
        // Display a greeting with the entered username and request count
        echo "<h2>Hello, $username!</h2>";
    }

}

// Function to find the index of a user in the array or add a new entry
function findUserIndex(&$userData, $username) {
    foreach ($userData as $index => $user) {
        if ($user['username'] === $username) {
            return $index;
        }
    }

    // If the user is not found, find an empty slot or use the first slot
    $emptyIndex = array_search(['username' => '', 'requestCount' => 0], $userData);
    $userIndex = ($emptyIndex !== false) ? $emptyIndex : 0;

    return $userIndex;
}
?>
