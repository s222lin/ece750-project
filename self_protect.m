yalmip('clear')
clear all


N = 5;
nx = 4;
nu = 5;
% count_mal, count_total, resource, annoyance
x = sdpvar(repmat(nx,1,N+1),repmat(1,1,N+1));
% increase_resource, decrease_resource, reauthentication, block, throttle
u = intvar(repmat(nu,1,N),repmat(1,1,N));

constraints = [];
objective   = 0;

A = eye(4);

B = [0 0 0 -1 0;
    0 0 0 -1 0;
    1 -1 0 0 0;
    0 0 0.5 0 0.5;];

Q = [1/200*0.1 0 0 0;
    0 1/200*0.1 0 0;
    0 0 1/3*0.6 0;
    0 0 0 0.2;];
r = [0;200;0;0;];
R = 0;


for k = 1:N
    objective = objective  + (x{k}-r)'*Q*(x{k}-r);
    % range of x
    constraints = [constraints, [0;0;0;0;] <= x{k} <= [200;200;3;1]];
    constraints = [constraints, [0;0;0;0;0;] <= u{k} <= [1;1;1;200;1]];

    constraints = [constraints, x{k+1} == A*x{k}+B*u{k} ];

    % response time
    %constraints = [constraints, x{k+1}(1) == ((x{k}(5) - x{k}(4)*u{k}(4))/(x{k}(6) + u{k}(1) - u{k}(2)))/(x{k}(5)/x{k}(6))*x{k}(1) ];
    
    % perc_mal
    %constraints = [constraints, x{k+1}(4) == x{k}(4)*(1-u{k}(4)) ];
    % user
    %constraints = [constraints, x{k+1}(5) == x{k}(5) - x{k}(4)*u{k}(4) ];
    % resource
    %constraints = [constraints, x{k+1}(6) == x{k}(6) + u{k}(1) - u{k}(2) ];
    % annoynance
    %constraints = [constraints, x{k+1}(7) == u{k}(3) ];
end
constraints = [constraints, [0;0;0;0;] <= x{k+1} <= [200;200;3;1]];
controller = optimizer(constraints, objective, sdpsettings('verbose',1),x{1},u{1});

fileID = fopen('data.txt','r');
xk = fscanf(fileID,'%f');
% xk = [20;200;0;0];
deltaT = 1;
totalTime = 10;

count_mal = [];
count_total= []; 
resource= []; 
annoyance= [];

uk = controller{xk};
count_mal = [count_mal, xk(1)];
count_total = [count_total, xk(2)];
resource = [resource, xk(3)];
annoyance = [annoyance, xk(4)];
xk = A*xk+B*uk;


fileID = fopen('plan.txt','w');
fprintf(fileID,'%d\n',uk);
fclose(fileID);

