yalmip('clear')
clear all


N = 5;
nx = 4;
nu = 3;
% response_time, mal_user_time, resourece_time, limit_time
x = sdpvar(repmat(nx,1,N+1),repmat(1,1,N+1));
% delte_mal, delte_resource, delte_limit
u = sdpvar(repmat(nu,1,N),repmat(1,1,N));



fileID = fopen('data.txt','r');
xk_all = fscanf(fileID,'%f');

for i = 1:4:400
    
    xk = xk_all(i:i+3,:);
    disp(xk)
    A = [1 0 0 0;
        0 1 0 0;
        0 0 1 0;
        0 0 0 1 ;];
    
    B = [-1 -1 -1 ;
        -1 0 0;
        0 -1 0;
        0 0 -1;];

    C = [1/xk(1) 0 0 0;
        0 1/(xk(2))*0.58 0 1/(xk(4))*0.42;
        0 0 1/xk(3) 0;
        0 1/(xk(2))*0.7 0 0 ;];

    D = [0 0 0 ;
        0 0 0;
        0 0 0;
        0 0 -1/xk(4)*0.3;];
    
    Q = [0.15 0 0 0;
        0 0.6 0 0;
        0 0 0.1 0;
        0 0 0 0.15;];
    R = [0 0 0;
        0 0 0;
        0 0 0.3;];
    r = [200/xk(1)*0.15;0;0.1;0.15;];
    
    
    constraints = [];
    objective   = 0;
    for k = 1:N
        objective = objective  + ((C*x{k}+D*u{k})-r)'*Q*((C*x{k}+D*u{k})-r);
        %objective = objective  + (x{k}-rx)'*Q*(x{k}-rx);
        % range of x
        constraints = [constraints, [0;0;0;0;] <= x{k} <= [xk(1);xk(2);xk(3);xk(4)]];
        constraints = [constraints, [0;0;0] <= u{k} <= [x{k}(2);x{k}(3);x{k}(4)]];
        constraints = [constraints,  u{k}(1) + u{k}(2) + u{k}(3)  <= x{k}(1)-200];
    
        constraints = [constraints, x{k+1} == A*x{k}+B*u{k}];
    end
    constraints = [constraints, [0;0;0;0;] <= x{k+1} <= [xk(1);xk(2);xk(3);xk(4)]];
    controller = optimizer(constraints, objective, sdpsettings('verbose',1),x{1},u{1});
    

    
    uk = controller{xk};

    xk = A*xk+B*uk;
    
    
    fileID = fopen('plan.txt','a');
    fprintf(fileID,'%f\n',uk);
    fclose(fileID);
    
end


